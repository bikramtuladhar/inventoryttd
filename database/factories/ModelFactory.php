<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

$factory->define(
    App\User::class,
    function (Faker\Generator $faker) {
        static $password;

        return [
            'name'           => $faker->name,
            'email'          => $faker->unique()->safeEmail,
            'password'       => $password ?: $password = bcrypt('secret'),
            'remember_token' => str_random(10),
        ];
    }
);

$factory->define(
    App\Item::class,
    function (Faker\Generator $faker) {
        return [
            'name'        => 'Nokia XL',
            'brand'       => 'Nokia',
            'category'    => 'Electronics',
            'color'       => 'black',
            'weight'      => '17',
            'description' => "Nokia's new Android phone",
            'stock'       => '100',
            'price'       => '1500000',
            'status'      => false,
            'added_date'  => Carbon::parse('December 13, 2016'),
        ];
    }
);
$factory->state(
    App\Item::class,
    'available',
    function (Faker\Generator $faker) {
        return ['status' => true];
    }
);
$factory->state(
    App\Item::class,
    'unavailable',
    function (Faker\Generator $faker) {
        return ['status' => false];
    }
);
