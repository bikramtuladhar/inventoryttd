<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'items',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('brand');
                $table->string('category');
                $table->string('color');
                $table->unsignedInteger('weight');
                $table->unsignedInteger('price');
                $table->string('description');
                $table->string('name');
                $table->unsignedInteger('stock');
                $table->boolean('status');
                $table->date('added_date');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
