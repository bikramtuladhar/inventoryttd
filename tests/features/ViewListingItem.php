<?php

use App\Item;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewListingItem extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * A basic test example.
     *
     * @return void
     */
    public function user_can_view_available_item()
    {
        $this->disableExceptionHandling();
        $item = factory(Item::class)->states('available')->create();
        $this->visit('/items/'.$item->id);
        $this->see("Nokia XL");
        $this->see("Nokia");
        $this->see("Electronics");
        $this->see("black");
        $this->see("17 gram");
        $this->see("Nrs 15000/-");
        $this->see("Nokia's new Android phone");
        $this->see("100 Pcs");
        $this->see("Available");
        $this->see("December 13, 2016");
    }

    /** @test * */
    public function user_cannot_view_unavailable_item()
    {
        $item = factory(Item::class)->states('unavailable')->create();
        $this->get('/items/'.$item->id);
        $this->assertResponseStatus(404);
    }
}
