# Laravel Inventory System

## Features

-list of roles
1. super admin != cannot have multiple superadmin
2. manager (multiple)
3. salesman (multiple)
4. users (multiple)

-available task for customer
1. Crud profile
2. can purchase items != cannot purchased unavailable or out of stock items
3. view or delete purchase history (soft delete )

-available task for sales man
1.accept purchase order from user
2.crud operation of customer (!+ cannot delete customer)

-available task for manager
--inhert from salesman
----can delete customer

1. crud operation of sales men
2. crud operation of items

-available task for superadmin
--inhert from manager
1. overall task for all roles

# Inventory features
- can store any kind of items
- have multiple category
- have limited stock capacity for each category


