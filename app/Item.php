<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];

    protected $dates = ['added_date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getFormattedWeightAttribute()
    {
        return $this->weight.$this->determineUnit();
    }

    public function getFormattedQuantityAttribute()
    {
        return $this->stock.$this->determineStockUnit();
    }

    public function getFormattedStatusAttribute()
    {
        return ($this->status) ? "Available" : 'Not Available';
    }

    public function getFormattedPriceAttribute()
    {
        $price_in_rupee = $this->price / 100;

        return 'Nrs '.$price_in_rupee.'/-';
    }

    public function getFormattedDateAttribute()
    {
        return $this->added_date->format('F j, Y');
    }

    public function determineUnit()
    {
        if ($this->category === 'Electronics') {
            return ' Grams';
        }
    }

    private function determineStockUnit()
    {
        if ($this->category === 'Electronics') {
            return ' Pcs';
        }
    }

    public function scopeAvailable($query)
    {
        return $query->where('status', true);
    }

}